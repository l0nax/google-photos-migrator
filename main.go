package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strconv"
	"strings"
	"sync"
	"sync/atomic"
	"time"

	"github.com/charmbracelet/bubbles/progress"
	tea "github.com/charmbracelet/bubbletea"
	"github.com/charmbracelet/lipgloss"
	"github.com/samber/lo"
)

type PhotoJSON struct {
	Title       string `json:"title"`
	Description string `json:"description"`

	PhotoTakenTime struct {
		Timestamp string `json:"timestamp"`
	} `json:"photoTakenTime"`

	GeoData struct {
		Latitude  float64 `json:"latitude"`
		Longitude float64 `json:"longitude"`
		Altitude  float64 `json:"altitude"`

		LatitudeSpan  float64 `json:"latitudeSpan"`
		LongitudeSpan float64 `json:"longitudeSpan"`
	} `json:"geoData"`
	GeoDataExif struct {
		Latitude      float64 `json:"latitude"`
		Longitude     float64 `json:"longitude"`
		Altitude      float64 `json:"altitude"`
		LatitudeSpan  float64 `json:"latitudeSpan"`
		LongitudeSpan float64 `json:"longitudeSpan"`
	} `json:"geoDataExif"`
}

const (
	padding  = 2
	maxWidth = 80
)

var totalFiles atomic.Uint64
var processedFiles atomic.Uint64

var helpStyle = lipgloss.NewStyle().Foreground(lipgloss.Color("#626262")).Render
var wg sync.WaitGroup

var p *tea.Program

type tickMsg time.Time

func tickCmd() tea.Cmd {
	return tea.Tick(100*time.Millisecond, func(t time.Time) tea.Msg {
		return tickMsg(t)
	})
}

type model struct {
	percent  float64
	progress progress.Model
}

func (m model) Init() tea.Cmd {
	return tickCmd()
}

var onceCloseFileQueue sync.Once

func closeFileQueue() {
	onceCloseFileQueue.Do(func() {
		close(fileQueue)
	})
}

func (m model) Update(msg tea.Msg) (tea.Model, tea.Cmd) {
	switch msg := msg.(type) {
	case tea.KeyMsg:
		closeFileQueue()

		return m, tea.Quit
	case tea.WindowSizeMsg:
		m.progress.Width = msg.Width - padding*2 - 4
		if m.progress.Width > maxWidth {
			m.progress.Width = maxWidth
		}

		return m, nil
	case tickMsg:
		m.percent = float64(processedFiles.Load()) / float64(totalFiles.Load())
		if m.percent >= 1.0 {
			return m, tea.Quit
		}

		return m, tickCmd()
	default:
		return m, nil
	}
}

func (m model) View() string {
	pad := strings.Repeat(" ", padding)
	return "\n" +
		pad + m.progress.ViewAs(m.percent) +
		fmt.Sprintf("  (%d / %d)\n", processedFiles.Load(), totalFiles.Load()) + "\n\n" +
		pad + helpStyle("Press any key to quit")
}

func main() {
	dir := os.Args[1]
	prog := progress.New(progress.WithScaledGradient("#FF7CCB", "#FDFF8C"))

	startProc()

	go func() {
		if err := filepath.Walk(dir, fileWalker); err != nil {
			log.Fatalf("Unable to exec: %+v\n", err)
		}
	}()

	if _, err := tea.NewProgram(model{progress: prog}).Run(); err != nil {
		fmt.Println("Oh no!", err)
		os.Exit(1)
	}

	closeFileQueue()
	wg.Wait()
}

type Vec []string

func (v *Vec) Push(str ...string) {
	if len(str) == 1 {
		*v = append(*v, str...)
		return
	} else if len(str) == 2 {
		*v = append(*v, fmt.Sprintf("%s=%q", str[0], str[1]))

		return
	}

	*v = append(*v, str...)
}

func (v Vec) Merge(args ...string) []string {
	return append(v, args...)
}

func fileWalker(path string, info os.FileInfo, err error) error {
	if err != nil {
		return err
	}

	if info.IsDir() || strings.EqualFold(filepath.Ext(path), ".json") {
		return nil
	}

	meta, metaPath, err := findMetaPath(path)
	if err != nil {
		log.Printf("[ERROR] Unable to read JSON: %v\n", err)

		return nil
	} else if metaPath == "" {
		// meta file not found
		return nil
	}

	totalFiles.Add(1)

	fileQueue <- task{
		path:     path,
		meta:     meta,
		metaPath: metaPath,
		info:     info,
	}

	return nil
}

const numWorker = 10

type task struct {
	path     string
	meta     PhotoJSON
	metaPath string
	info     os.FileInfo
}

var fileQueue = make(chan task, 16*1024)

func processWorker() {
	defer wg.Done()

	for task := range fileQueue {
		processFile(task.path, task.meta, task.metaPath, task.info)
	}
}

func startProc() {
	for i := 0; i < numWorker; i++ {
		wg.Add(1)
		go processWorker()
	}
}

func processFile(path string, meta PhotoJSON, metaPath string, info os.FileInfo) {
	defer func() {
		processedFiles.Add(1)
	}()

	ts, err := strconv.ParseInt(meta.PhotoTakenTime.Timestamp, 10, 64)
	if err != nil {
		log.Printf("[ERROR] Unable to read time on %q (%q): %+v\n", path, metaPath, meta)
		return
	}

	creationDate := time.Unix(ts, 0)
	if info.ModTime().Equal(creationDate) {
		return
	}

	var args Vec

	if meta.Description != "" {
		args.Push(fmt.Sprintf("-Comment=%q", meta.Description))
		args.Push(fmt.Sprintf("-ImageDescription=%q", meta.Description))
	}

	timeTaken := creationDate.Format(time.RFC3339)

	_, isQuickTime := quicktime[strings.ToLower(filepath.Ext(path))]
	if isQuickTime {
		args.Push("-DateTimeOriginal", timeTaken)
		args.Push("-CreateDate", timeTaken)
		args.Push("-ModifyDate", timeTaken)
		args.Push("-TrackCreateDate", timeTaken)
		args.Push("-TrackModifyDate", timeTaken)
		args.Push("-MediaCreateDate", timeTaken)
		args.Push("-MediaModifyDate", timeTaken)
	} else {
		args.Push("-SubSecDateTimeOriginal", timeTaken)
		args.Push("-SubSecCreateDate", timeTaken)
		args.Push("-SubSecCreateDate", timeTaken)
	}

	geo := meta.GeoData
	if geo.Latitude != 0 && geo.Altitude != 0 && geo.Longitude != 0 {
		gpsAlt := strconv.FormatFloat(geo.Altitude, 'g', -1, 64)
		args.Push("-GPSAltitude", gpsAlt)
		args.Push(fmt.Sprintf("-GPSAltitudeRef='%s'", gpsAlt))

		gpsLat := strconv.FormatFloat(geo.Latitude, 'g', -1, 64)
		args.Push("-GPSLatitude", gpsLat)
		args.Push(fmt.Sprintf("-GPSLatitudeRef='%s'", gpsLat))

		gpsLong := strconv.FormatFloat(geo.Longitude, 'g', -1, 64)
		args.Push("-GPSLongitude", gpsLong)
		args.Push(fmt.Sprintf("-GPSLongitudeRef='%s'", gpsLong))
	}

	cmd := exec.Command("exiftool", args.Merge("-overwrite_original", path)...)

	stderr, err := cmd.CombinedOutput()
	if err != nil {
		log.Printf("[ERROR] Unable to execute exiftool on %q:\n\t+%s\n++ stderr\n%s\n-----\n%+v\n", path,
			cmd.String(),
			string(stderr),
			err)
		// IGNORE!
		//os.Exit(1)

		return
	}

	// silently ignore it..
	if err := os.Chtimes(path, time.Now(), creationDate); err != nil {
		return
	}
}

func findMetaPath(path string) (pj PhotoJSON, pic string, err error) {
	paths := buildFilePaths(path)

	for _, metaPath := range paths {
		if _, err := os.Stat(metaPath); err == nil {
			data, err := os.ReadFile(metaPath)
			if err != nil {
				return pj, "", err
			}

			var meta PhotoJSON
			if err := json.Unmarshal(data, &meta); err != nil {
				return pj, "", err
			}

			return meta, metaPath, nil
		}
	}

	// Silent!
	//log.Printf("[WARN] File %q not mappable\n", path)

	return pj, "", nil
}

func buildFilePaths(path string) []string {
	ext := filepath.Ext(path)

	alias, ok := aliases[ext]
	if !ok {
		alias, ok = aliases[strings.ToLower(ext)]
	}

	ref := []string{ext, ""}
	ref = append(ref, alias...)

	genPotExt := func(ext string) []string {
		tmp := make([]string, 0, 5)
		//tmp = tmp[:0]

		// <name>(.<ext|extAlias>)?.json
		tmp = append(tmp, path+ext)

		nameCounterMatch := regexp.MustCompile(`(?P<name>.*)(?P<counter>\(\d+\))$`).FindStringSubmatch(path)
		var name, counter string
		for i, n := range nameCounterMatch {
			if n == "name" {
				name = nameCounterMatch[i]
			} else if n == "counter" {
				counter = nameCounterMatch[i]
			}
		}

		if name != "" && counter != "" {
			tmp = append(tmp, name+ext+counter)
		}

		if strings.HasSuffix(path, "_n-") || strings.HasSuffix(path, "_n") || strings.HasSuffix(path, "_") {
			tmp = append(tmp, path[:len(path)-1]+ext)
		}

		for i, potBase := range tmp {
			tmp[i] = potBase + ".json"
		}

		return tmp
	}

	all := make([]string, 0, 10)
	for _, ext := range ref {
		all = append(all, genPotExt(ext)...)
	}

	// Special Case #1
	// Given the file name "Screenshot_20190305_151808_com.google.android.y.jpg"
	// The resulting JSON file will be named: "Screenshot_20190305_151808_com.google.android..json"
	relPath := path[:len(path)-len(ext)]

	idx := strings.LastIndex(relPath, ".")
	if idx > 0 {
		newCand := relPath[:idx] // remove the .<some character>
		all = append(all, newCand+"..json")
	}

	// Special Case #2
	// Given the file name "original_ef68959a-2f1f-4fee-a996-bcff8373f333_S(1).png"
	//    OR "original_ef68959a-2f1f-4fee-a996-bcff8373f333_S.png"
	// The resulting JSON file will be named: "original_ef68959a-2f1f-4fee-a996-bcff8373f333_.json"
	idx = strings.LastIndex(path, "_")
	if idx > 0 {
		newCand := relPath[:(idx + 1)] // include the "_" character
		all = append(all, newCand+".json")
	}

	return lo.Uniq(all)
}

var aliases = map[string][]string{
	".mp4": []string{".heic", ".jpg", ".jpeg"},
	".mov": []string{".heic", ".jpg", ".jpeg"},
}

var quicktime = map[string]struct{}{
	".mp4":    struct{}{},
	".mov":    struct{}{},
	".heic":   struct{}{},
	".heif":   struct{}{},
	".qt":     struct{}{},
	".mov.qt": struct{}{},
	".mp4v":   struct{}{},
	".3gp":    struct{}{},
}
